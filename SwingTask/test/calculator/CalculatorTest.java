package calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import calculator.Calculator.Operation;


public class CalculatorTest {
	private Calculator calculator;

	@Before
	public void setUp() throws Exception {
		calculator = new Calculator();
	}

	@Test
	public void testListToStringWorkFine() {
		List<String> list = new ArrayList<>();
		final String twoSeparators = System.lineSeparator() + System.lineSeparator();
		list.add("10 / 5 = 2");
		list.add("7 - 5 = 2");
		list.add("15 + 6 = 21");
		list.add("5 * 5 = 25");
		String expected = "5 * 5 = 25" + twoSeparators + "15 + 6 = 21" + twoSeparators + "7 - 5 = 2" + twoSeparators
				+ "10 / 5 = 2";
		Collections.reverse(list);
		String actual = calculator.listToString(list);
		assertEquals(expected, actual);
		
		assertEquals("",calculator.listToString(null));
		assertEquals("",calculator.listToString(new ArrayList<>()));
	}
	
	@Test
	public void testOperationEnumEqualsWorkFine() {
		assertTrue(Operation.ADD.titleEqualsTo("+"));
		assertTrue(Operation.DIV.titleEqualsTo("/"));
		assertTrue(Operation.SUB.titleEqualsTo("-"));
		assertTrue(Operation.MULT.titleEqualsTo("*"));
	}
	
	@Test
	public void testIsInutDataWorkFine() {
		assertTrue(calculator.isInputDataValid("5"));
		assertTrue(calculator.isInputDataValid("5."));
		assertTrue(calculator.isInputDataValid("0.1"));
		assertTrue(calculator.isInputDataValid("-0.1"));
		assertTrue(calculator.isInputDataValid("-.1"));
		assertTrue(calculator.isInputDataValid("-6."));
		
		assertFalse(calculator.isInputDataValid("-6.a"));
		assertFalse(calculator.isInputDataValid("@"));
		assertFalse(calculator.isInputDataValid(" "));
	}
	
	@Test
	public void testPlusOperationDoCalculateWorkFine() {
		calculator.getFirstOperandText().setText("5");
		calculator.getSecondOperandText().setText("5");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(0);
		calculator.doCalculate();
		
		String expectedHistory = "5 + 5 = 10";
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult = "10";
		assertEquals(expectedResult, calculator.getResultTextField().getText());
		
		calculator.getFirstOperandText().setText("a");
		calculator.getSecondOperandText().setText("5");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(0);;
		calculator.doCalculate();
		
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult2 = "Wrong input. Insert the number!";
		assertEquals(expectedResult2, calculator.getResultTextField().getText());
	}
	
	@Test
	public void testSubOperationDoCalculateWorkFine() {
		calculator.getFirstOperandText().setText("5");
		calculator.getSecondOperandText().setText("5");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(1);
		calculator.doCalculate();
		
		String expectedHistory = "5 - 5 = 0";
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult = "0";
		assertEquals(expectedResult, calculator.getResultTextField().getText());
	}
	
	@Test
	public void testMultOperationDoCalculateWorkFine() {
		calculator.getFirstOperandText().setText("5");
		calculator.getSecondOperandText().setText("5");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(2);
		calculator.doCalculate();
		
		String expectedHistory = "5 * 5 = 25";
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult = "25";
		assertEquals(expectedResult, calculator.getResultTextField().getText());
	}
	
	@Test
	public void testDivOperationDoCalculateWorkFine() {
		calculator.getFirstOperandText().setText("5");
		calculator.getSecondOperandText().setText("5");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(3);
		calculator.doCalculate();
		
		String expectedHistory = "5 / 5 = 1";
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult = "1";
		assertEquals(expectedResult, calculator.getResultTextField().getText());
		
		calculator.getFirstOperandText().setText("5");
		calculator.getSecondOperandText().setText("0");
		calculator.getMenuWithOperationsCombo().setSelectedIndex(3);
		calculator.doCalculate();
		
		assertEquals(expectedHistory, calculator.getHistoryText().getText());
		
		String expectedResult2 = "Division by zero";
		assertEquals(expectedResult2, calculator.getResultTextField().getText());
	}

}
