package calculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

public class Calculator {
	private JFrame jFrame;

	private List<String> historyList = new ArrayList<>();

	private JTextField firstOperandText;
	private JTextField secondOperandText;
	private JTextArea historyText;

	private JComboBox<String> menuWithOperationsCombo;

	private JButton calculateButton;
	private JCheckBox onFlyButton;

	private JTextField resultTextField;
	private Pattern p = Pattern.compile("(^[\\+\\-]?[0-9]*[.]?[0-9]+$)|(^[\\+\\-]?[0-9]+[.]?$)");


	public Calculator() {
		this.jFrame = initFrame();
		initCalculator();
	}

	public JFrame initFrame() {
		JFrame frame = new JFrame();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimension = toolkit.getScreenSize();
		int height = 300;
		int width = 320;
		frame.setBounds(dimension.width / 2 - width / 2, dimension.height / 2 - height / 2, width, height);
		frame.setTitle("Calculator");
		return frame;
	}

	public void initCalculator() {
		JTabbedPane jTabbedPane = new JTabbedPane(SwingConstants.TOP);

		jTabbedPane.addTab("Calculator", createCalculateTab());
		jTabbedPane.addTab("History", createHistoryTab());

		jFrame.getContentPane().setLayout(new GridLayout());
		jFrame.getContentPane().add(jTabbedPane);
		jFrame.revalidate();

	}

	private JPanel createCalculateTab() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 1, 5, 5));

		//
		// First level
		//

		Container firstLevelContainer = new Container();
		firstLevelContainer.setLayout(new GridBagLayout());

		GridBagConstraints firstLevelconstraints = new GridBagConstraints();
		firstLevelconstraints.fill = GridBagConstraints.HORIZONTAL;
		firstLevelconstraints.insets = new Insets(0, 3, 0, 3);
		firstLevelconstraints.weightx = 1;
		firstLevelconstraints.gridy = 0;

		firstOperandText = new JTextField();
		firstOperandText.setName("first operand");
		firstOperandText.setToolTipText("Enter an integer or floating point number");
		firstOperandText.addCaretListener(new CustomModifyListenerForTextOfOperand());
		firstOperandText.setBorder(BorderFactory.createEtchedBorder());
		firstOperandText.setText("first operand");
		firstOperandText.setHorizontalAlignment(SwingConstants.CENTER);
		firstOperandText.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (firstOperandText.getText().equals("first operand")) {
					firstOperandText.setText("");
					firstOperandText.setHorizontalAlignment(SwingConstants.LEFT);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (firstOperandText.getText().length() == 0) {
					firstOperandText.setText("first operand");
					firstOperandText.setHorizontalAlignment(SwingConstants.CENTER);
				}
			}

		});
		firstLevelconstraints.gridx = 0;
		firstLevelconstraints.ipady = 7;
		firstLevelconstraints.ipadx = 120;
		firstLevelContainer.add(firstOperandText, firstLevelconstraints);

		menuWithOperationsCombo = new JComboBox<>(
				Arrays.copyOf(Arrays.stream(Operation.values()).map(x -> x.title).toArray(), Operation.values().length,
						String[].class));
		menuWithOperationsCombo.setToolTipText("select operation");
		menuWithOperationsCombo.addActionListener(e -> {
			if (onFlyButton.getSelectedObjects() != null)
				doCalculate();
		});

		firstLevelconstraints.gridx = 1;
		firstLevelconstraints.ipady = 0;
		firstLevelconstraints.ipadx = 0;
		firstLevelContainer.add(menuWithOperationsCombo, firstLevelconstraints);

		secondOperandText = new JTextField();
		secondOperandText.setName("second operand");
		secondOperandText.setToolTipText("Enter an integer or floating point number");
		secondOperandText.addCaretListener(new CustomModifyListenerForTextOfOperand());
		secondOperandText.setBorder(BorderFactory.createEtchedBorder());
		secondOperandText.setText("second operand");
		secondOperandText.setHorizontalAlignment(SwingConstants.CENTER);
		secondOperandText.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (secondOperandText.getText().equals("second operand")) {
					secondOperandText.setText("");
					secondOperandText.setHorizontalAlignment(SwingConstants.LEFT);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (secondOperandText.getText().length() == 0) {
					secondOperandText.setText("second operand");
					secondOperandText.setHorizontalAlignment(SwingConstants.CENTER);
				}
			}

		});

		firstLevelconstraints.gridx = 2;
		firstLevelconstraints.ipady = 7;
		firstLevelconstraints.ipadx = 120;
		firstLevelContainer.add(secondOperandText, firstLevelconstraints);

		panel.add(firstLevelContainer);
		//
		// End of First level
		//

		Container separatorContainer = new Container();
		separatorContainer.setLayout(new GridLayout(0, 1));
		panel.add(separatorContainer);

		//
		// Second level
		//

		Container secondLevelContainer = new Container();
		secondLevelContainer.setLayout(new GridBagLayout());

		GridBagConstraints secondLevelConstraints = new GridBagConstraints();
		secondLevelConstraints.insets = new Insets(0, 3, 0, 3);
		secondLevelConstraints.fill = GridBagConstraints.HORIZONTAL;
		secondLevelConstraints.weighty = 1;
		secondLevelConstraints.weightx = 1;

		onFlyButton = new JCheckBox("Calculate on the fly");
		onFlyButton.setToolTipText("Automatic calculate without clicking the Calculate button");
		onFlyButton.addActionListener(e -> {
			JCheckBox source = (JCheckBox) e.getSource();
			if (source.getSelectedObjects() != null) {
				calculateButton.setEnabled(false);
				doCalculate();
			} else {
				calculateButton.setEnabled(true);
			}
		});

		secondLevelContainer.add(onFlyButton, secondLevelConstraints);

		calculateButton = new JButton("Calculate");
		calculateButton.addActionListener(e -> doCalculate());
		secondLevelContainer.add(calculateButton, secondLevelConstraints);

		panel.add(secondLevelContainer);

		//
		// End of Second level
		//

		//
		// Third level
		//

		Container thirdLevelContainer = new Container();
		thirdLevelContainer.setLayout(new GridBagLayout());

		GridBagConstraints thirdLevelConstraints = new GridBagConstraints();
		thirdLevelConstraints.insets = new Insets(0, 3, 0, 3);
		thirdLevelConstraints.fill = GridBagConstraints.CENTER;
		thirdLevelConstraints.weighty = 1;
		thirdLevelConstraints.weightx = 1;

		JLabel resultTextLabel = new JLabel("Result:");
		thirdLevelContainer.add(resultTextLabel, thirdLevelConstraints);

		resultTextField = new JTextField();
		resultTextField.setEditable(false);
		resultTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		resultTextField.setBorder(BorderFactory.createEtchedBorder());
		thirdLevelConstraints.fill = GridBagConstraints.HORIZONTAL;
		thirdLevelConstraints.ipady = 7;
		thirdLevelConstraints.ipadx = 220;

		thirdLevelContainer.add(resultTextField, thirdLevelConstraints);

		panel.add(thirdLevelContainer);
		//
		// End of Third level
		//

		return panel;
	}

	private JPanel createHistoryTab() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		historyText = new JTextArea();
		panel.add(new JScrollPane(historyText), BorderLayout.CENTER);
		historyText.setEditable(false);
		return panel;

	}

	private void makeAnswerForHistoryList(JComboBox<String> combo, JTextArea historyText, JTextField labelResult,
			double oper1, double oper2, double res) {
		DecimalFormat format = new DecimalFormat("#,###.########");
		historyList.add(format.format(oper1) + " " + combo.getSelectedItem() + " " + format.format(oper2) + " = "
				+ format.format(res));
		historyText.setText(listToString(historyList));
		labelResult.setText(format.format(res));
	}

	protected String listToString(List<String> list) {
		if (list == null) {
			return "";
		}
		List<String> copyList = new ArrayList<>();
		copyList.addAll(list);
		final String separator = System.lineSeparator();
		return copyList.stream().collect(Collectors.joining(separator + separator)).trim();
	}

	protected boolean isInputDataValid(String input) {
		Matcher matcher = p.matcher(input);
		return matcher.matches();
	}

	protected void doCalculate() {
		if (firstOperandText.getText() != null && firstOperandText.getText().length() != 0
				&& secondOperandText.getText() != null && secondOperandText.getText().length() != 0
				&& menuWithOperationsCombo.getSelectedIndex() >= 0) {
			double oper1;
			double oper2;
			double res;
			try {
				if (isInputDataValid(firstOperandText.getText()) && isInputDataValid(secondOperandText.getText())) {
					oper1 = Double.parseDouble(firstOperandText.getText());
					oper2 = Double.parseDouble(secondOperandText.getText());
				} else {
					if (firstOperandText.getText().equals("first operand")
							|| secondOperandText.getText().equals("second operand")) {
						return;
					}
					throw new NumberFormatException();
				}
			} catch (NumberFormatException exception) {
				resultTextField.setText("Wrong input. Insert the number!");
				return;
			}

			String operation = menuWithOperationsCombo.getSelectedItem().toString();
			if (Operation.ADD.titleEqualsTo(operation)) {
				res = oper1 + oper2;
				makeAnswerForHistoryList(menuWithOperationsCombo, historyText, resultTextField, oper1, oper2, res);
			} else if (Operation.SUB.titleEqualsTo(operation)) {
				res = oper1 - oper2;
				makeAnswerForHistoryList(menuWithOperationsCombo, historyText, resultTextField, oper1, oper2, res);
			} else if (Operation.MULT.titleEqualsTo(operation)) {
				res = oper1 * oper2;
				makeAnswerForHistoryList(menuWithOperationsCombo, historyText, resultTextField, oper1, oper2, res);
			} else if (Operation.DIV.titleEqualsTo(operation)) {
				if (oper2 != 0) {
					res = oper1 / oper2;
					makeAnswerForHistoryList(menuWithOperationsCombo, historyText, resultTextField, oper1, oper2, res);
				} else {
					resultTextField.setText("Division by zero");
				}
			}
		}
	}

	private class CustomModifyListenerForTextOfOperand implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent e) {
			JTextField text = (JTextField) e.getSource();
			if (isInputDataValid(text.getText()) || text.getText().length() == 0) {
				text.setBackground(Color.white);
				resultTextField.setText("");
			} else {
				if (text.getText().equals("first operand") || text.getText().equals("second operand")) {
					return;
				}
				text.setBackground(Color.red);
				resultTextField.setText("Wrong input. Insert the number!");
			}

			if (onFlyButton.getSelectedObjects() != null) {
				doCalculate();
			}
		}

	}

	/**
	 * Enum for initializing arithmetic operations. The title contains the
	 * arithmetic operation symbol.
	 */
	protected enum Operation {
		ADD("+"), SUB("-"), MULT("*"), DIV("/");

		/**
		 * Contain title of element
		 */
		private String title;

		/**
		 * @param title field which must contain title of element
		 */
		Operation(String title) {
			this.title = title;
		}

		/**
		 * Checking if title and value are equal
		 * 
		 * @param value which to be compared against the enum title
		 * @return returns true if the value is equal to the title, returns false if the
		 *         value is not equal to the title
		 */
		public boolean titleEqualsTo(String value) {
			return title.equals(value);
		}
	}

	public JTextField getSecondOperandText() {
		return secondOperandText;
	}

	public JTextArea getHistoryText() {
		return historyText;
	}

	public JComboBox<String> getMenuWithOperationsCombo() {
		return menuWithOperationsCombo;
	}

	public JTextField getResultTextField() {
		return resultTextField;
	}

	public JTextField getFirstOperandText() {
		return firstOperandText;
	}
	
}
